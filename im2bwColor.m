function segm = im2bwColor(image, threshold)
    r = threshold(1);
    g = threshold(2);
    b = threshold(3);
    t = threshold(4);
    o = im2double(image);
    sr = o(:,:,1) < r + t & o(:,:,1) > r - t;
    sg = o(:,:,2) < g + t & o(:,:,2) > g - t;
    sb = o(:,:,3) < b + t & o(:,:,3) > b - t;
    segm = sr & sg & sb;  
    %o1 = im2double(o); % konwersja obrazu na typ double >> imshow(o1); impixelinfo; % wy�wietl obraz, pokaz warto�ci punkt�w >> r = 0.95 % warto�ci przyk�adowe >> g = 0.88 >> b = 0.22 >> [m n n1] = size(o1); >> rgb = cat(3,r*ones(m,n),g*ones(m,n),b*ones(m,n)); >> imshow(rgb); >> d1 = (o1 - rgb) .* (o1 - rgb); >> dist = sqrt ( d1(:,:,1) + d1(:,:,2) + d1(:,:,3)); >> imshow(dist); % odleg�o�ci do wybranego koloru >> p = 0.3 % prog odleglosci >> segm = dist < p; >> imshow (segm);
end