function [c, liczbaklas] = pobierz_wzor(i)
    i = imclose(i,ones(5)); % filtracja morfologiczna
    t1 = 0.7; % prog binaryzacji
    i = im2bw(i,t1); % segmentacja przez progowanie
    c2 = matcech(i);
    c = c2(1,:); %pobieranie wektora cech wzorca
    liczbaklas = size(c,1);
end