function [image, mask, wynik] = znajdz_wzor(o, cechy_wzor, threshold, prog)
    %barwy poprawnych i niepoprawnych obiektow
    red =   [1 0 0];
    green = [0 1 0];
    
    %VARIABLES
    koror =  zeros(7,3); %macierz kolorow poszczegolnych obiektow

    wynik = struct;
    wynik.correct = 0;
    wynik.incorrect = 0;
    wynik.unknown = 0;

    o2 = im2bwColor(o,threshold);
    o2 = imclearborder(o2);
    %imshow(o2);

    % wyznaczanie opisu obrazu (wektor�w cech)
    [o3 lab] = bwlabel(o2); % etykietuj;
    cechy_znakow = matcech(o2);
    
    for k=1:1:lab % dla wszystkich obiektow znalezionych na obrazie wykonuj
        aktualny = cechy_znakow(k,:); % wekt. cech aktualnie rozpozn.znaku
        d = sqrt(sum(abs(cechy_wzor - repmat(aktualny,1,1)).^2,2)) % odleglosci od poprawnego elementu      
        if d <= prog
            %disp('poprawny');
            koror(k+1,:) = green;
            wynik.correct = wynik.correct+1;
        else 
            %disp('obiekt pominiety');
            koror(k+1,:) = red;
            wynik.incorrect = wynik.incorrect+1;
        end
    end 

    image = ind2rgb(o3+1,koror);
    mask = o2;
end