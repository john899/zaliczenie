%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              Rozpoznawanie element�w w obrazie, projekt 2               %
%                                                                         %
%            Przetwarzanie i Rozpoznawanie Obrazu 2017/2017               %
%                             27.06.2018r                                 %
%                          Jan Jeromin 265935                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;clc;clf;close all;

% wartosc progowa odleglosci wektora cech od wzorca (jak bardzo podobny ma byc obiekt do wzorca)
prog = 0.1;
%wielkosc okienka filtra morfologicznego
flr = 5;

%trashold interesujacych kolorow (r,g,b,t)
orange_threshold =  [0.90,0.80,0.20,0.3];
black_threshold  =  [0.20,0.20,0.20,0.2];
blue_threshold   =  [0.20,0.20,0.80,0.3];

%pobieranie obrazu wzoru i do analizy
wzor = imread('wzor.tif');
imin = imread('elementy5_5.tif');

%pobieranie cech z przykladowego obrazu
cechy_wzor = pobierz_wzor(wzor);

% filtracja morfologiczna
iminFiltered = imclose(imin,ones(flr)); 

[image, liczba] = koloruj_skrajne(iminFiltered);

%znajdowanie wynik�w dla kolejnych barw
[image1, mask1, wynik_c] = znajdz_wzor(iminFiltered, cechy_wzor, black_threshold, prog); %czarne
[image2, mask2, wynik_b] = znajdz_wzor(iminFiltered, cechy_wzor, blue_threshold, prog);  %niebieske
[image3, mask3, wynik_o] = znajdz_wzor(iminFiltered, cechy_wzor, orange_threshold, prog);%pomaranczowe

%��czenie otrzymanych obraz�w wynikowych w jeden
imout = polacz_obrazy(image, image1, mask1);
imout = polacz_obrazy(imout, image2, mask2);
imout = polacz_obrazy(imout,  image3, mask3);

%liczenie poprawnych i niepoprawnych ogolnie
poprawne = wynik_c.correct+  wynik_b.correct + wynik_o.correct;
niepoprawne = wynik_c.incorrect + wynik_b.incorrect + wynik_o.incorrect;
niesklasyfikowane = liczba - (poprawne + niepoprawne);

%wyswietlanie wynik�w
fprintf('Liczba obiektow: %d\n', liczba);
fprintf('Poprawne: %d\n', poprawne);
fprintf('Niepoprawne: %d\n', niepoprawne);
fprintf('Nieskalsyfikowane: %d\n', niesklasyfikowane);
fprintf('\n');
fprintf('Czarne: poprawne: %d, niepoprawne: %d\n',wynik_c.correct, wynik_c.incorrect);
fprintf('Niebieskie: poprawne: %d, niepoprawne: %d\n',wynik_b.correct, wynik_b.incorrect);
fprintf('Pomara�czowe: poprawne: %d, niepoprawne: %d\n',wynik_o.correct, wynik_o.incorrect);


figure;
imshow([imin iminFiltered]);
title('Obraz pocz�tkowy oraz po filtracji');

figure;
subplot(1,3,1), subimage(image1)
title('Czarne');
subplot(1,3,2), subimage(image2)
title('Niebieskie');
subplot(1,3,3), subimage(image3)
title('Pomara�czowe');

figure;
imshow(imout);