function mout= matcech(i)
    [j liczbaklas] = bwlabel(i);
    %imshow(j)
    %imshow(j+1,rand(100,3))
    cechy = regionprops(j, 'BoundingBox','Centroid','EulerNumber','Extent');
    euler = cat(1,cechy.EulerNumber); % konwersja pola EulerNumber struktury w macierz
    extent = cat(1,cechy.Extent); % j.w. pola Extend
    bb = cat(1,cechy.BoundingBox); % j.w. pola BoundingBox
    centroid = cat(1,cechy.Centroid); % j.w. pola Centroid
    cposx = (centroid(:,1)-bb(:,1))./bb(:,3); % wzgledna pozycja 'x' sr.ciezkosci
    cposy = (centroid(:,2)-bb(:,2))./bb(:,4); % wzgledna pozycja 'y' sr.ciezkosci
    c = [euler extent cposx cposy]; % ostateczne wektory cech znakow
    mout = c;
end