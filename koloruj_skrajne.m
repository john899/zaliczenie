function [image, lab] = koloruj_skrajne(imin)
    o2 = im2bw(imin,0.7);
    o2 = imcomplement(o2);
    [out lab] = bwlabel(o2);
    koror =  ones(lab + 1,3);
    koror(1,:) = [0 0 0];
    image = ind2rgb(out+1,koror);
end