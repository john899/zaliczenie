function [obraz, maska] = polacz_obrazy(obraz1, obraz2, mask) 
    im3a = im2double(mask); %konwersja maski do double
    im3b(:,:,1) = im3a; % maska w wersji kolorowej - sk�adowa R
    im3b(:,:,2) = im3a; % sk�adowa G
    im3b(:,:,3) = im3a; % sk�adowa B   

    im3c = imcomplement(im3b); % odwr�cona maska 
    
    im5 = min(obraz2, im3b);
    im4 = min(obraz1, im3c); 
    im6 = max(im4,im5); 
    
    obraz = im6;
    maska = im3a;
end